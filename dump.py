import subprocess
import shlex
import logging
import os
import json
from collections import OrderedDict
import glob


def dumpPDBs():
    logger = logging.getLogger("dumpPDBs")
    with open("dump.tcl", "w") as f:
        f.write("""mol new prmtop type parm7
mol addfile 03_Prod_reimage.mdcrd type crdbox waitfor -1

file mkdir pdbs/
set nf [molinfo top get numframes]
for {set i 0} {$i < $nf} {incr i} {
set a [atomselect top "(resname NHE or protein) and noh" frame $i]
$a writepdb pdbs/$i.pdb
}

quit
""")
    cmd = "vmd -dispdev text -e dump.tcl"
    logger.debug("Running command '" + cmd + "'")
    proc = subprocess.Popen(shlex.split(cmd), shell=False)
    proc.wait()
    # Dump all to one file
    os.chdir("pdbs")
    os.system("cat $(ls -tr) > ../all.pdb")
    os.chdir("../")


def determineSS():
    logger = logging.getLogger("determineSS(dump.py)")

    logger.debug("Running DSSP on structures")
    pdbs = glob.glob("pdbs/*.pdb")
    try:
        os.mkdir("dssp")
    except:
        pass

    for pdb in pdbs:
        pfile = pdb.replace("pdbs/", "")
        os.system(
            "../bin/dssp-2.0.4-linux-amd64 -i pdbs/%(pdb)s -o dssp/%(pdb)s.ss" % {'pdb': pfile})

    logger.debug("Parsing DSSP of structures")
    try:
        os.remove("ss.dat")
    except OSError:
        pass

    # Code	Description
    # H	Alpha helix
    # B	Beta bridge
    # E	Strand
    # G	Helix-3
    # I	Helix-5
    # T	Turn
    # S	Bend

    pdbs = glob.glob("dssp/*.ss")
    maxSecondaryStructure = -1
    bestStructureSS = ""
    ss = OrderedDict({"H": 0, "I": 0, "T": 0, "E": 0,
                      "B": 0, "C": 0, "S": 0, "G": 0})
    with open("ss.dat", "a") as fout:
        fout.write("# ")
        for ssType in ss:
            fout.write(ssType + " ")
        fout.write("\n")
    for pdb in pdbs:
        try:
            lines = open(pdb, "r").read().splitlines()
        except:
            continue
        startProcessing = False
        ss = OrderedDict({"H": 0, "I": 0, "T": 0, "E": 0,
                          "B": 0, "C": 0, "S": 0, "G": 0})
        for line in lines:
            if "#  RESIDUE" in line:
                startProcessing = True
                continue
            if not startProcessing:
                continue
            ssRes = line[16:17]
            if ssRes is " ":
                continue
            ss[ssRes] += 1
        with open("ss.dat", "a") as fout:
            fout.write(pdb.replace(".pdb.ss", "").replace("dssp/", "") + " ")
            for ssType in ss:
                fout.write(str(ss[ssType]) + " ")
            fout.write("\n")
        secondaryStructure = ss["H"] + ss["B"] + ss["E"] + \
            ss["G"] + ss["I"]  # only turns and bends don't count
        if secondaryStructure > maxSecondaryStructure:
            maxSecondaryStructure = secondaryStructure
            bestStructureSS = pdb.replace(".ss", "").replace("dssp/", "")

    logger.debug("cp pdbs/%(pdb)s bestSS_%(val)s_%(pdb)s" %
                 {"pdb": bestStructureSS, "val": str(maxSecondaryStructure)})
    os.system("cp pdbs/%(pdb)s bestSS_%(val)s_%(pdb)s" %
              {"pdb": bestStructureSS, "val": str(maxSecondaryStructure)})


if __name__ == "__main__":
    dumpPDBs()
