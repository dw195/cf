import shlex
import subprocess
import os
import logging


def calculateEnergy(num):
    sNum = str(num)
    logger = logging.getLogger("findEnergy")
    try:
        os.system("rm -rf temp")
    except:
        pass
    os.system("mkdir temp/")
    os.system("cp ./pdbs/%s.pdb temp/protein.pdb" % sNum)
    os.chdir("temp")

    logger.info("Creating prmtop/inpcrd from pdb")
    tleapConf = """source leaprc.ff14SB
    loadAmberParams frcmod.ionsjc_tip3p
    mol = loadpdb protein.pdb
    saveamberparm mol prmtop inpcrd
    quit"""
    with open("tleap.foo","w") as f:
        f.write(tleapConf)
    cmd = "tleap -f tleap.foo"
    logger.debug("Running command '" + cmd + "'")
    proc = subprocess.Popen(shlex.split(cmd),shell=False)
    proc.wait()
    os.remove("tleap.foo")

    with open("01_Min.in","w") as f:
        f.write("""Minimize
     &cntrl
      imin=1,
      ntx=1,
      irest=0,
      maxcyc=10,
      ncyc=5,
      ntpr=5,
      ntwx=0,
      cut=10.0,
      ntb=0,
     /
    """)
    cmd = "%(amber)s/bin/sander -O -i %(cwd)s/01_Min.in -o %(cwd)s/01_Min.out -p %(cwd)s/prmtop -c %(cwd)s/inpcrd -r %(cwd)s/01_Min.rst -inf %(cwd)s/01_Min.mdinfo" % {'cwd':os.getcwd(),'amber':os.environ['AMBERHOME']}
    proc = subprocess.Popen(shlex.split(cmd),shell=False)
    proc.wait()

    nums = []
    for line in open("01_Min.mdinfo","r"):
    	for number in line.split():
    		try:
    			nums.append(str(float(number)))
    		except:
    			pass
    os.chdir("../")
    with open("energies.dat","a") as f:
        f.write(sNum + " " + " ".join(nums) + "\n")


if __name__ == "__main__":
    for i in range(1,1310):
	calculateEnergy(i)
