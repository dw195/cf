import json
import glob
import subprocess
import shlex
import os

def calculateAlignment(templateFile = "../template.pdb"):
    maxResNum = 0
    minResNum = 1000
    try:
        with open(templateFile,'r') as f:
            for line in f:
                s = list(line)
                if ''.join(s[0:4]).strip() == "ATOM":
                    res = int(''.join(s[23:27]))
                    if res > maxResNum:
                        maxResNum = res
                    if res < minResNum:
                        minResNum = res
    except:
        with open('template.pdb','r') as f:
            for line in f:
                s = list(line)
                if ''.join(s[0:4]).strip() == "ATOM":
                    res = int(''.join(s[23:27]))
                    if res > maxResNum:
                        maxResNum = res
                    if res < minResNum:
                        minResNum = res

    maxResNum2 = 0
    minResNum2 = 1000
    with open('pdbs/0.pdb','r') as f:
        for line in f:
            s = list(line)
            if ''.join(s[0:4]).strip() == "ATOM":
                res = int(''.join(s[23:27]))
                if res > maxResNum2:
                    maxResNum2 = res
                if res < minResNum2:
                    minResNum2 = res

    alignMaxRes = maxResNum
    alignMinRes = minResNum
    if maxResNum2 < maxResNum:
        alignMaxRes = maxResNum2-1
    if minResNum2 > minResNum:
        alignMinRes = minResNum2

    pdbs = glob.glob("./pdbs/*.pdb")
    rmsds = {}
    for pdb in pdbs:
        with open('temp','w') as f:
            f.write("""mol new %(pdb)s
mol new %(templateFile)s
set sel1 [atomselect 0 "backbone and resid %(res1)s to %(res2)s"]
set sel2 [atomselect 1 "backbone and resid %(res1)s to %(res2)s"]
set transformation_matrix [measure fit $sel1 $sel2]
set move_sel [atomselect 0 "all"]
$move_sel move $transformation_matrix
set a [measure rmsd $sel1 $sel2]
set fo [open "vmddump" "w"]
puts $fo $a
close $fo
exit""" % {"pdb":pdb,"res1":alignMinRes,"res2":alignMaxRes,"templateFile":templateFile} )
        cmd = "vmd -dispdev text -e temp"
        proc = subprocess.Popen(shlex.split(cmd),shell=False)
        proc.wait()
        rmsds[pdb] = float(open('vmddump','r').read())	
        with open("rmsds.dat","a") as f:
             f.write(pdb.replace("./pdbs/","").replace(".pdb","") + " " + str(rmsds[pdb]) + "\n")
    os.remove("temp")
    os.remove("vmddump")
    with open("rmsds.json","w") as f:
        f.write(json.dumps(rmsds))



if __name__ == "__main__":
    calculateAlignment("template.pdb")
    try:
        os.remove("rmsds.dat")
    except:
        pass
    a = json.load(open('rmsds.json','r'))
    for i in range(1,2000):
        si = "./pdbs/"+ str(i) + ".pdb"
        if si in a:
            with open("rmsds.dat","a") as f:
                f.write(str(a[si]))
                f.write("\n")
        else:
            break
